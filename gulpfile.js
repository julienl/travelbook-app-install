var gulp = require('gulp')
	surge = require('gulp-surge');

gulp.task('deploy', [], function () {
  return surge({
    project: './',         // Path to your static build directory
    domain: 'travelbook.surge.sh'  // Your domain or Surge subdomain
  })
})